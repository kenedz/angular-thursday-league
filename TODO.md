#Features

## Improvements

### Season page
- kazdy hrac muze byt v jednom zapase jenom jednou
- upravit match detail a vymenit grid za neco lepsiho, zaroven pri prazdne soupisce pridat '-'
- yes/no dialog na remove

### Create dialog
- Validace skore na 4. strance

### Ranking page
- pridat sortovani na vsechny sloupce

### Match dialog
- seradit hrace podle abecedy a mit prijmeni jako prvni
- validace na vysledek zapasu, pokud nejsou vybrani hraci

## Tasks

### Pridavani sezon
- sezonu by pridaval admin z admin page

### Pridavani lig
- ligu by pridaval admin z admin page

### Globalni veci
- error handling
- theming aplikace

### User page
- zavest osobni statistiky - graf levelu v case, graf poradi v case atd.

### Zavest eslint

### Zavest jednotneho klienta pro posilani requestu

#Bugs

## Update Ranking db tabulky, neobsahuje season_id

## Problem s casem ulozeni zapasu
- pokud ulozim zapas, cas se ulozi spatne, chce to zrusit i sekundy (ulozim zapas na 18:00 a v db je 17:00 nebo 16:00)
- promisy a jejich den nahradit za async/await

## Pri vytvoreni nebo updatovani matche mne ui vyhazuje exceptionu, ze nedokaze parsovat json

## Razeni hracu v rankingu je rozbite
