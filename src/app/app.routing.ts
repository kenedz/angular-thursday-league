import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';

import {MainMenuComponent} from './main-menu/main-menu.component';

const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {
        path: '', component: MainMenuComponent, children: [{
            path: '', loadChildren: './main-menu/main-menu.module#MainMenuModule'
        }]
    }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, {
            useHash: true,
            onSameUrlNavigation: 'reload'
        })
    ],
    exports: []
})
export class AppRoutingModule {
}
