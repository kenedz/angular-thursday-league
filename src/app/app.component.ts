import {Component} from '@angular/core';
import {LoginStore} from './core/local-store/LoginStore';
import {TranslateStore} from "./core/local-store";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(private _loginService: LoginStore, private translateService: TranslateStore) {
    }

    userSignedOut(): boolean {
        return this._loginService.signedOut();
    }

}
