import {Component, OnInit, ViewChild} from '@angular/core';
import {RankingService, Ranking, Player} from '../core/services';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Utils} from "../core";
import {SeasonsStore} from "../core/local-store";

@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    public maxPageSize = 25;

    public dataSource: MatTableDataSource<Ranking>;
    public displayedColumns: string[] = ['player', 'level', 'rank', 'pointsavg', 'pointsoverall', 'played', 'missed', 'wins', 'draws',
        'loses', 'score', 'scoredoverall', 'scoredavg', 'receivedoverall', 'receivedavg'];

    private _navigationSubscription: any;
    private _rankings: Ranking[];

    constructor(private _rankingService: RankingService, private _seasonStore: SeasonsStore, private _route: ActivatedRoute, private _router: Router) {
        this._navigationSubscription = this._router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                this.ngOnInit();
            }
        });
    }

    ngOnInit() {
        const seasonId = this._route.snapshot.params['season'];

        this._rankingService.getRankingsBySeasonId(Number.parseInt(seasonId)).then(result => {
            this._rankings = result;
            Utils.sortRankings(this._rankings);
            this.dataSource = new MatTableDataSource<Ranking>(this._rankings);
            this.dataSource.paginator = this.paginator;
        })
    }

    public getName(ranking: Ranking): string {
        let player: Player = ranking.player;
        return player.firstName + ' ' + player.lastName + ' (' + player.email + ')';
    }

    public getMaxRating(): number {
        return Ranking.MAX_RATING;
    }

    public getScore(ranking: Ranking): number {
        return ranking.scoredOverall - ranking.receivedOverall;
    }

    public getSeasonYear(): string {
        const seasonId = this._route.snapshot.params['season'];
        const season = this._seasonStore.getSeasonById(Number.parseInt(seasonId));

        return season ? season.seasonYear : ' N/A ';
    }
}
