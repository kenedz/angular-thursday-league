import {Component, OnInit} from '@angular/core';
import {LeaguesStore, LoginStore, TranslateStore} from '../core/local-store';
import {SeasonMatch} from "../seasons/models/SeasonMatch";
import {MatchesUtils} from "../core/services/matches/MatchesUtils";
import {MatchDetailDialogComponent} from "../seasons/components/match-detail-dialog/match-detail-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {User, Match, RankingService, Ranking, MatchesService, League} from "../core/services";
import {MatTableDataSource} from "@angular/material/table";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    private readonly _lastMatchesNum = 5;

    private _bestPlayersCounter = 1;

    public bestRankings: Ranking[];
    public rankingPlayer: Ranking = new Ranking();
    public mostPlayedRankings: Ranking[];

    public dataSource: MatTableDataSource<SeasonMatch>;
    public displayedColumns: string[] = ['detail', 'matchDate', 'player'];
    public lastMatches: SeasonMatch[];

    constructor(private _translateService: TranslateStore, private _loginService: LoginStore,
                private _rankingService: RankingService, private _matchesService: MatchesService, private _leaguesStore: LeaguesStore,
                public dialog: MatDialog) {

    }

    public async ngOnInit() {
        await this._initDashboardData();
    }

    private async _initDashboardData() {
        this._bestPlayersCounter = 1;

        const league: League = this._leaguesStore.getAppLeague();
        if (league) {
            const rankings = await this._rankingService.getCurrentRankingsByLeagueId(league.leagueId);
            let currentUser = this._loginService.getAppUser();
            this._getPersonalData(currentUser, rankings);
            this._getBestPlayers(rankings);
            this._getMostPlayed(rankings);

            let matches = await this._matchesService.getCurrentSeasonMatchesByLeagueId(league.leagueId);
            matches = MatchesUtils.sortMatchesByDate(matches, false);
            let lastMatchesArr = new Array<Match>();
            let index = 0;
            for (let match of matches) {
                if (index < this._lastMatchesNum) {
                    lastMatchesArr.push(match);
                }

                index++;
            }

            this.lastMatches = MatchesUtils.convertToSeasonMatches(lastMatchesArr);
            this.dataSource = new MatTableDataSource<SeasonMatch>(this.lastMatches);
        }
    }

    private _getPersonalData(currentUser: User, rankings: Ranking[]) {
        for (let ranking of rankings) {
            let player = ranking.player;

            if (currentUser.email === player.email) {
                this.rankingPlayer = ranking;
                break;
            }
        }
    }

    private _getBestPlayers(rankings: Ranking[]) {
        let bestRankings = new Array<Ranking>();
        rankings = rankings.sort((a, b) => {
            return this._compareByOrder(a.pointsAvg, b.pointsAvg);
        });

        let maxPlayers = 5;
        let playersCounts = 0;
        for (let i = 0; i < rankings.length; i++) {
            if (i === 0) {
                bestRankings.push(rankings[i]);
                playersCounts++;
                continue;
            }

            if (playersCounts < maxPlayers) {
                bestRankings.push(rankings[i]);
                playersCounts++;
            } else {
                if (rankings[i - 1].pointsAvg === rankings[i].pointsAvg) {
                    bestRankings.push(rankings[i]);
                    playersCounts++
                } else {
                    break;
                }
            }
        }

        this.bestRankings = bestRankings;
    }

    private _getMostPlayed(rankings: Ranking[]) {
        let mostPlayed = new Array<Ranking>();
        rankings = rankings.sort((a, b) => {
            return this._compareByPlayed(a.played, b.played);
        });

        let maxPlayers = 5;
        let playersCounts = 0;
        for (let i = 0; i < rankings.length; i++) {
            if (i === 0) {
                mostPlayed.push(rankings[i]);
                playersCounts++;
                continue;
            }

            if (playersCounts < maxPlayers) {
                mostPlayed.push(rankings[i]);
                playersCounts++;
            } else {
                if (rankings[i - 1].played === rankings[i].played) {
                    mostPlayed.push(rankings[i]);
                    playersCounts++
                } else {
                    break;
                }
            }
        }

        this.mostPlayedRankings = mostPlayed;
    }

    private _compareByOrder(pointsA: number, pointsB: number) {
        return pointsB - pointsA;
    }

    private _compareByPlayed(playedA: number, playedB: number) {
        return playedB - playedA;
    }

    public openDetail(seasonMatch: SeasonMatch) {
        this.dialog.open(MatchDetailDialogComponent, {
            data: {seasonMatch: seasonMatch}
        });
    }

}
