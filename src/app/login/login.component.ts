import {Component, OnInit} from '@angular/core';
import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginStore} from '../core/local-store/LoginStore';
import {UsersService} from '../core/services/users/users.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    user: SocialUser;
    showUnauthorized = false;
    returnUrl: string;

    constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router, private loginService: LoginStore,
                private usersService: UsersService) {
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.showUnauthorized = false;
        this.returnUrl = this.route.snapshot.paramMap.get('returnUrl') || '/';
    }

    signInWithGoogle(): void {
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(socialUser => {
            this.usersService.getUserByEmail(socialUser.email).then(user => {
                if (socialUser && !user) {
                    this.showUnauthorized = true;
                    this.user = socialUser;
                    this.signOut();
                } else {
                    this.showUnauthorized = false;
                    this.storeLogin(socialUser);

                    this.router.navigateByUrl(this.returnUrl);
                }
            });
        });
    }

    storeLogin(user: SocialUser): void {
        this.loginService.storeLogin(user);
    }

    signOut(): void {
        this.authService.signOut().then(value => {
            this.loginService.removeStorage();
        });
    }

}
