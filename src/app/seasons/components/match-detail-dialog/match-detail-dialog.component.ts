import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatchDialogInput} from '../../seasons.component';
import {Lineup} from '../../../core/services';

@Component({
    selector: 'app-match-dialog',
    templateUrl: './match-detail-dialog.component.html',
    styleUrls: ['./match-detail-dialog.component.scss']
})
export class MatchDetailDialogComponent implements OnInit {

    public matchFormGroup: FormGroup;

    constructor(private _dialogRef: MatDialogRef<MatchDetailDialogComponent>, @Inject(MAT_DIALOG_DATA) public dialogInput: MatchDialogInput) {
        this.matchFormGroup = new FormGroup({
            homeTeamGoalsControl: new FormControl('', [Validators.required])
        });
    }


    public ngOnInit() {
    }

    public onCancelClick(): void {
        this._dialogRef.close();
    }

    public getResult() {
        let homeScored: number = this.dialogInput.seasonMatch.match.homeScored;
        let awayScored: number = this.dialogInput.seasonMatch.match.awayScored;
        if (!homeScored || !awayScored) {
            return ' - ';
        }

        return homeScored + ' - ' + awayScored;
    }

    public getHomePlayers(): string {
        let lineups = this.dialogInput.seasonMatch.match.lineup;

        if (!lineups) {
            return 'N/A';
        }

        let homePlayers = new Array<Lineup>();
        lineups.forEach(lineup => {
            if (lineup.team === Lineup.HOME_TEAM) {
                homePlayers.push(lineup);
            }
        });

        return this._mergeStringsWithDelimiter(homePlayers, ', ');
    }

    public getAwayPlayers(): string {
        let lineups = this.dialogInput.seasonMatch.match.lineup;

        if (!lineups) {
            return 'N/A';
        }

        let awayPlayers = new Array<Lineup>();
        lineups.forEach(lineup => {
            if (lineup.team === Lineup.AWAY_TEAM) {
                awayPlayers.push(lineup);
            }
        });

        return this._mergeStringsWithDelimiter(awayPlayers, ', ');
    }

    public onSubmit() {

    }

    private _mergeStringsWithDelimiter(lineups: Lineup[], delimiter: string): string {
        let result: string = '';

        for (let lineup of lineups) {
            result = result + lineup.player.firstName + ' ' + lineup.player.lastName + delimiter;
        }

        result = result.substring(0, result.lastIndexOf(delimiter));

        return result;
    }

}
