import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchDetailDialogComponent } from './match-detail-dialog.component';

describe('MatchDialogComponent', () => {
  let component: MatchDetailDialogComponent;
  let fixture: ComponentFixture<MatchDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
