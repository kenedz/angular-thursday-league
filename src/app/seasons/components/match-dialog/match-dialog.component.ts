import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CreateMatchDialogInput} from '../../seasons.component';
import {TranslateStore} from '../../../core/local-store';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Player, Lineup, Match} from '../../../core/services';
import {SeasonMatch} from '../../models/SeasonMatch';
import {SelectItem} from 'primeng';
import {DialogOperations} from "../../../admin-console/models/DialogOperations";
import {Utils} from "../../../core";

@Component({
    selector: 'app-create-match-dialog',
    templateUrl: './match-dialog.component.html',
    styleUrls: ['./match-dialog.component.scss']
})
export class MatchDialogComponent implements OnInit {

    public matchFormGroup: FormGroup;

    public matchDate: Date = new Date();

    public homeTeamPlayers: SelectItem[];
    public awayTeamPlayers: SelectItem[] = [];
    public selectedHomeTeamPlayers: SelectItem[] = [];
    public selectedAwayTeamPlayers: SelectItem[] = [];

    public homeTeamScore: string = '';
    public awayTeamScore: string = '';

    constructor(private _dialogRef: MatDialogRef<MatchDialogComponent>, @Inject(MAT_DIALOG_DATA) public dialogInput: CreateMatchDialogInput,
                private _translateService: TranslateStore, private _formBuilder: FormBuilder) {
        this.matchFormGroup = new FormGroup({
            matchDateTimeControl: new FormControl({value: ''}),
        });
    }

    public ngOnInit() {
        this.homeTeamPlayers = this._getListboxHomeTeamPlayers(this.dialogInput.players);

        if (this.dialogInput.mode === DialogOperations.UPDATE) {
            this._updateDialogValues();
        }
    }

    private _getListboxHomeTeamPlayers(players: Player[]): SelectItem[] {
        let listboxPlayers = new Array<SelectItem>();

        players.forEach(player => {
            listboxPlayers.push(this._getListboxHomeTeamPlayer(player));
        });

        return listboxPlayers.sort((a, b) => {
            return this.compareByLabel(a.label, b.label, true);
        });
    }

    private _getListboxHomeTeamPlayer(player: Player): SelectItem {
        let listboxPlayer: SelectItem = {label: this._getLabel(player), value: player};
        return listboxPlayer;
    }

    private _getLabel(player: Player): string {
        return player.lastName + ' ' + player.firstName + ' (' + player.email + ')';
    }

    private compareByLabel(a: string, b: string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    public onCancelClick(): void {
        this._dialogRef.close();
    }

    public onOK() {
        let seasonMatch: SeasonMatch = this.dialogInput.seasonMatch;
        if (this.dialogInput.mode === DialogOperations.UPDATE) {
            let matchId: number = seasonMatch.match.matchId;
            seasonMatch.match.matchId = matchId;
        } else if (this.dialogInput.mode === DialogOperations.CREATE) {
            seasonMatch.match = new Match();
        }
        seasonMatch.match.matchDate = this.matchDate;
        seasonMatch.match.homeScored = Utils.isNumeric(this.homeTeamScore) ? Number(this.homeTeamScore) : undefined;
        seasonMatch.match.awayScored = Utils.isNumeric(this.awayTeamScore) ? Number(this.awayTeamScore) : undefined;

        seasonMatch.match.lineup = this._getMatchLineup();

        this.dialogInput.isFinished = true;
    }

    public onNextInHomeTeamStepper(): void {
        this.awayTeamPlayers = new Array<SelectItem>();

        for (let homePlayer of this.homeTeamPlayers) {
            let isSelected: boolean = false;
            for (let selectedPlayer of this.selectedHomeTeamPlayers) {
                if (homePlayer.value.email === selectedPlayer.value.email) {
                    isSelected = true;
                    break;
                }
            }

            if (isSelected === false) {
                this.awayTeamPlayers.push(homePlayer);
            }
        }
    }

    public getLineupHome(): string {
        let lineupHome = '';
        const delimiter = ', ';

        this.selectedHomeTeamPlayers.forEach(player => {
            let p: Player = player.value;
            lineupHome = lineupHome + p.firstName + ' ' + p.lastName + delimiter;
        });

        return lineupHome.length > 0 ? lineupHome.substring(0, lineupHome.lastIndexOf(delimiter)) : ' - ';
    }

    public getLineupAway(): string {
        let lineupAway = '';
        const delimiter = ', ';

        this.selectedAwayTeamPlayers.forEach(player => {
            let p: Player = player.value;
            lineupAway = lineupAway + p.firstName + ' ' + p.lastName + delimiter;
        });

        return lineupAway.length > 0 ?lineupAway.substring(0, lineupAway.lastIndexOf(delimiter)) : ' - ';
    }

    private _getMatchLineup(): Lineup[] {
        let lineups: Lineup[] = new Array<Lineup>();

        this.selectedHomeTeamPlayers.forEach(player => {
            let linup: Lineup = new Lineup();

            linup.player = player.value;
            linup.team = Lineup.HOME_TEAM;

            lineups.push(linup);
        });

        this.selectedAwayTeamPlayers.forEach(player => {
            let linup: Lineup = new Lineup();

            linup.player = player.value;
            linup.team = Lineup.AWAY_TEAM;

            lineups.push(linup);
        });

        return lineups;
    }

    private _updateDialogValues(): void {
        let seasonMatch: SeasonMatch = this.dialogInput.seasonMatch;

        this.matchDate = new Date(seasonMatch.match.matchDate);

        this.selectedHomeTeamPlayers = this._getHomePlayers(seasonMatch.match.lineup);
        this.selectedAwayTeamPlayers = this._getAwayPlayers(seasonMatch.match.lineup);

        this.homeTeamScore = Utils.isNumeric(seasonMatch.match.homeScored) ? seasonMatch.match.homeScored.toLocaleString() : '';
        this.awayTeamScore = Utils.isNumeric(seasonMatch.match.awayScored) ? seasonMatch.match.awayScored.toLocaleString() : '';
    }

    private _getHomePlayers(lineups: Lineup[]): SelectItem[] {
        let listboxPlayers = new Array<SelectItem>();

        for (let lineup of lineups) {
            if (lineup.team === Lineup.HOME_TEAM) {
                for (let listboxPlayer of this.homeTeamPlayers) {
                    if (lineup.player.email === listboxPlayer.value.email) {
                        listboxPlayers.push(listboxPlayer);
                    }
                }
            }
        }

        return listboxPlayers;
    }

    private _getAwayPlayers(lineups: Lineup[]): SelectItem[] {
        let listboxPlayers = new Array<SelectItem>();

        for (let lineup of lineups) {
            if (lineup.team === Lineup.AWAY_TEAM) {
                for (let listboxPlayer of this.homeTeamPlayers) {
                    if (lineup.player.email === listboxPlayer.value.email) {
                        listboxPlayers.push(listboxPlayer);
                    }
                }
            }
        }

        return listboxPlayers;
    }

    public get matchDateTimeControl() {
        return this.matchFormGroup.get('matchDateTimeControl');
    }

}
