import {Match} from '../../core/services/matches/models/Match';

export class SeasonMatch {
    public static readonly WIN = 'WIN';
    public static readonly LOSE = 'LOSE';
    public static readonly DRAW = 'DRAW';
    public static readonly TBD = 'TBD';

    id: number;
    match: Match;
}
