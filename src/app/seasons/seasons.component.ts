import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {LoginStore, RankingsStore, SeasonsStore, TranslateStore} from '../core/local-store';
import {MatchesService, Match, PlayersService, Player, Lineup, Ranking, RankingService} from '../core/services';
import {MatPaginator} from '@angular/material/paginator';
import {Sort} from '@angular/material/sort';
import {SeasonMatch} from './models/SeasonMatch';
import {MatchDetailDialogComponent} from './components/match-detail-dialog/match-detail-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {MatchDialogComponent} from './components/match-dialog/match-dialog.component';
import {MatchesUtils} from "../core/services/matches/MatchesUtils";
import {DialogOperations} from "../admin-console/models/DialogOperations";
import {Utils} from "../core";

@Component({
    selector: 'app-seasons',
    templateUrl: './seasons.component.html',
    styleUrls: ['./seasons.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ])
    ]
})
export class SeasonsComponent implements OnInit {

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    public maxPageSize = 25;

    private _navigationSubscription: any;

    public dataSource: MatTableDataSource<SeasonMatch> = new MatTableDataSource<SeasonMatch>();
    public expandedElement: SeasonMatch | null;
    public expand: boolean;
    public displayedAllColumns: string[] = ['detail', 'matchDate', 'result'];
    public displayedStaticColumns: string[] = ['detail', 'matchDate', 'result'];
    public displayedDynamicColumns: string[];
    public title: string;

    private _seasonMatches: SeasonMatch[];
    private _players: Player[];
    private _seasonId: number;

    constructor(private _route: ActivatedRoute, private _router: Router, private _translateService: TranslateStore,
                private _matchesService: MatchesService, private _playersService: PlayersService,
                private _loginService: LoginStore, public _dialog: MatDialog, private _rankingService: RankingService,
                private _rankingsStore: RankingsStore, private _seasonStore: SeasonsStore) {

        this.expand = true;

        this._navigationSubscription = this._router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                this.ngOnInit();
            }
        });
    }

    public ngOnInit() {
        this._seasonId = Number.parseInt(this._route.snapshot.params['season']);

        this._translateService.getTranslation('page.season.title').subscribe(seasonTitle => {
            this.title = seasonTitle + ' ' + this._seasonStore.getSeasonById(this._seasonId).seasonYear;
        });

        this._matchesService.getMatchesBySeasonId(this._seasonId).then(matches => {
            matches = MatchesUtils.sortMatchesByDate(matches, false);

            this._seasonMatches = MatchesUtils.convertToSeasonMatches(matches);

            this.dataSource = new MatTableDataSource<SeasonMatch>(this._seasonMatches);
            this.dataSource.paginator = this.paginator;

            this._playersService.getPlayers().then(players => {
                this._players = players;

                this.displayedAllColumns.length = 0;

                this.displayedDynamicColumns = this.createNameColumns(players);
                this.displayedAllColumns = this.displayedStaticColumns.concat(this.displayedDynamicColumns);

                /** When the match is created or updated, refreshRankings flag is updated to true a rankings are refreshed */
                if (this._rankingsStore.refreshRankings()) {
                    this._refreshRanking();
                    this._rankingsStore.storeRefreshRankings(false);
                }
            });
        });

    }

    public openCreateDialog() {
        this.expand = !this.expand;

        const dialogRef = this._dialog.open(MatchDialogComponent, {
            data: {
                players: this._players,
                seasonMatch: new SeasonMatch(),
                mode: DialogOperations.CREATE,
                isFinished: false
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && result.isFinished) {
                const match =  result.seasonMatch.match;
                match.seasonId = this._seasonId;
                this._matchesService.createMatch(match).then(() => {
                    this._rankingsStore.storeRefreshRankings(true);
                    window.location.reload();
                });
            }
        });
    }

    public updateMatch(seasonMatch: SeasonMatch) {
        const dialogRef = this._dialog.open(MatchDialogComponent, {
            data: {
                players: this._players,
                seasonMatch: seasonMatch,
                mode: DialogOperations.UPDATE,
                isFinished: false
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && result.isFinished) {
                const match =  result.seasonMatch.match;
                match.seasonId = this._seasonId;
                this._matchesService.updateMatch(match).then(() => {
                    this._rankingsStore.storeRefreshRankings(true);
                    window.location.reload();
                });
            }
        });
    }

    public deleteMatch(seasonMatch: SeasonMatch) {
        this._matchesService.deleteMatch(seasonMatch.match.matchId.toLocaleString()).then(() => {
            this._rankingsStore.storeRefreshRankings(true);
            window.location.reload();
        });
    }

    sortData(sort: Sort) {
        const data = this._seasonMatches.slice();
        if (!sort.active || sort.direction === '') {
            this._seasonMatches = data;
            return;
        }

        this._seasonMatches = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            if (sort.active === 'matchDate') {
                return MatchesUtils.compareByDate(a.match.matchDate, b.match.matchDate, isAsc);
            } else {
                return 0;
            }
        });

        this.dataSource = new MatTableDataSource<SeasonMatch>(this._seasonMatches);
    }

    private createNameColumns(players: Player[]): string[] {
        Utils.sortPlayers(players);

        let namesColumns = new Array<string>();

        players.forEach(player => {
            namesColumns.push(this.getColumnName(player));
        });

        return namesColumns;
    }

    public getColumnName(player: Player): string {
        return player.firstName + ' ' + player.lastName + ' (' + player.email + ')';
    }

    public isCurrentUserAdmin(): boolean {
        return this._loginService.isCurrentUserAdmin();
    }

    public getResult(seasonMatch: SeasonMatch): string {
        if (!Utils.isNumeric(seasonMatch.match.homeScored)  || !Utils.isNumeric(seasonMatch.match.awayScored)) {
            return 'TBD';
        }
        return seasonMatch.match.homeScored + ' : ' + seasonMatch.match.awayScored;
    }

    public getResultForLineupPlayer(seasonMatch: SeasonMatch, columnName: string): string {
        let email: string = this._parseEmailFromColumnName(columnName);
        const notAvailabile = 'N/A';

        let lineupPlayer: Lineup;

        if (!seasonMatch.match.lineup) {
            return notAvailabile;
        }

        for (let lineup of seasonMatch.match.lineup) {
            if (lineup.player.email === email) {
                lineupPlayer = lineup;
                break;
            }
        }

        if (!lineupPlayer) {
            return notAvailabile;
        }

        let lineupPlayerResult: string;

        if (lineupPlayer.team === Lineup.HOME_TEAM) {
            if (seasonMatch.match.homeScored > seasonMatch.match.awayScored) {
                lineupPlayerResult = SeasonMatch.WIN;
            } else if (seasonMatch.match.homeScored < seasonMatch.match.awayScored) {
                lineupPlayerResult = SeasonMatch.LOSE;
            } else if (!seasonMatch.match.homeScored || !seasonMatch.match.awayScored) {
                lineupPlayerResult = SeasonMatch.TBD;
            } else {
                lineupPlayerResult = SeasonMatch.DRAW;
            }
        } else {
            if (seasonMatch.match.homeScored > seasonMatch.match.awayScored) {
                lineupPlayerResult = SeasonMatch.LOSE;
            } else if (seasonMatch.match.homeScored < seasonMatch.match.awayScored) {
                lineupPlayerResult = SeasonMatch.WIN;
            } else if (!seasonMatch.match.homeScored || !seasonMatch.match.awayScored) {
                lineupPlayerResult = SeasonMatch.TBD;
            } else {
                lineupPlayerResult = SeasonMatch.DRAW;
            }
        }

        return lineupPlayerResult;
    }

    private _parseEmailFromColumnName(columnName: string): string {
        if (!columnName) {
            return '';
        }

        return columnName.substring(columnName.indexOf('(') + 1, columnName.indexOf(')')).trim();
    }

    public getBackground(seasonMatch: SeasonMatch, columnName: string): string {

        let result: string = this.getResultForLineupPlayer(seasonMatch, columnName);
        if (result === SeasonMatch.WIN) {
            return 'win-background';
        } else if (result === SeasonMatch.LOSE) {
            return 'lose-background';
        } else if (result === SeasonMatch.DRAW) {
            return 'draw-background';
        } else if (result === SeasonMatch.TBD) {
            return 'tbd-background';
        }

        return '';
    }

    public openDetail(seasonMatch: SeasonMatch) {
        this.expand = !this.expand;

        const dialogRef = this._dialog.open(MatchDetailDialogComponent, {
            data: {seasonMatch: seasonMatch}
        });

        dialogRef.afterClosed().subscribe(() => {
            this.expand = !this.expand;
        });
    }

    public onRowClick(seasonMatch: SeasonMatch) {
        if (this.expand) {
            this.expandedElement = this.expandedElement === seasonMatch ? null : seasonMatch;
        }
    }

    private async _refreshRanking(): Promise<void> {
        const oldRankings = await this._rankingService.getRankingsBySeasonId(this._seasonId);
        const newRankings = this._calculateRankings();

        this._rankingService.createRankings(this._createOrUpdateRankings(oldRankings, newRankings));
    }

    private _calculateRankings(): Ranking[] {
        const rankings: Ranking[] = [];

        for (const player of this._players) {
            let ranking = new Ranking();
            ranking.seasonId = this._seasonId;
            ranking.player = player;

            let pointsOverall = 0;
            let played = 0;
            let missed = 0;
            let wins = 0;
            let draws = 0;
            let loses = 0;
            let scoredOverall = 0;
            let receivedOverall = 0;

            for (let seasonMatch of this._seasonMatches) {
                let match = seasonMatch.match;

                if (!Utils.isNumeric(match.homeScored) || !Utils.isNumeric(match.awayScored)) {
                    continue;
                }

                let lineup = this._getLineupPlayer(match, player);

                if (!lineup) {
                    missed++;
                } else {
                    played++;

                    if (lineup.team === Lineup.HOME_TEAM) {
                        if (match.homeScored > match.awayScored) {
                            // home win
                            wins++;
                            pointsOverall = pointsOverall + Match.POINTS_PER_WIN;
                        } else if (match.homeScored < match.awayScored) {
                            // away win
                            loses++;
                        } else {
                            // draw
                            draws++;
                            pointsOverall = pointsOverall + Match.POINTS_PER_DRAW;
                        }

                        scoredOverall = scoredOverall + match.homeScored;
                        receivedOverall = receivedOverall + match.awayScored;
                    } else {
                        if (match.homeScored > match.awayScored) {
                            // home win
                            loses++;
                        } else if (match.homeScored < match.awayScored) {
                            // away win
                            wins++;
                            pointsOverall = pointsOverall + Match.POINTS_PER_WIN;
                        } else {
                            // draw
                            draws++;
                            pointsOverall = pointsOverall + Match.POINTS_PER_DRAW;
                        }

                        scoredOverall = scoredOverall + match.awayScored;
                        receivedOverall = receivedOverall + match.homeScored;
                    }
                }

            }

            ranking.pointsOverall = pointsOverall;
            ranking.played = played;
            ranking.missed = missed;
            ranking.wins = wins;
            ranking.draws = draws;
            ranking.loses = loses;
            ranking.scoredOverall = scoredOverall;
            ranking.receivedOverall = receivedOverall;

            rankings.push(ranking);
        }

        return this._calculateRestFields(rankings);

    }

    private _createOrUpdateRankings(oldRankings: Ranking[], newRankings: Ranking[]): Ranking[] {
        if (!oldRankings || oldRankings.length === 0 || (oldRankings.length !== newRankings.length)) {
            return newRankings;
        }

        for (let i = 0; i < oldRankings.length; i++) {
            const oldRanking = oldRankings[i];
            const newRanking = newRankings[i];
            newRanking.rankingId = oldRanking.rankingId;
        }

        return newRankings;
    }

    private _getLineupPlayer(match: Match, player: Player): Lineup {
        for (let lineup of match.lineup) {
            if (lineup.player.email === player.email) {
                return lineup;
            }
        }

        return null;
    }

    private _calculateRestFields(rankings: Ranking[]): Ranking[] {

        for (let ranking of rankings) {
            if (!ranking.played || ranking.played === 0) {
                ranking.scoredAvg = 0.0;
                ranking.receivedAvg = 0.0;
                ranking.pointsAvg = 0.0;
            } else {
                ranking = this._updateScoredAverage(ranking);
                ranking = this._updateReceivedAverage(ranking);
                ranking = this._updatePoints(ranking);
            }
        }

        rankings = this._updateRank(rankings);
        rankings = this._updateLevel(rankings);

        return rankings;
    }

    private _updateScoredAverage(ranking: Ranking): Ranking {
        ranking.scoredAvg = ranking.scoredOverall / ranking.played;
        return ranking;
    }

    private _updateReceivedAverage(ranking: Ranking): Ranking {
        ranking.receivedAvg = ranking.receivedOverall / ranking.played;
        return ranking;
    }

    private _updatePoints(ranking: Ranking): Ranking {
        ranking.pointsAvg = ranking.pointsOverall / ranking.played;
        return ranking;
    }

    private _updateRank(rankings: Ranking[]): Ranking[] {
        rankings = Utils.sortRankings(rankings);

        for (let i = 0; i < rankings.length; i++) {
            let order = i + 1;

            if (i === 0) {
                rankings[i].rank = order;
                continue;
            }

            let previousRanking = rankings[i - 1];
            let currentRanking = rankings[i];

            if (previousRanking.pointsAvg === currentRanking.pointsAvg) {
                currentRanking.rank = previousRanking.rank;
            } else {
                currentRanking.rank = order;
            }
        }

        return rankings;
    }

    private _updateLevel(rankings: Ranking[]): Ranking[] {
        let lastInRank = this._getLastInRank(rankings);
        let firstInRank = this._getFirstInRank(lastInRank, rankings);

        for (let ranking of rankings) {
            ranking.level = this._calculateLevel(lastInRank, firstInRank, ranking.rank);
        }

        return rankings;
    }

    private _getLastInRank(rankings: Ranking[]): number {
        let last = 0;
        for (let ranking of rankings) {
            if (last < ranking.rank) {
                last = ranking.rank;
            }
        }

        return last;
    }

    private _getFirstInRank(lastInRank: number, rankings: Ranking[]): number {
        let first = lastInRank;
        for (let ranking of rankings) {
            if (first > ranking.rank) {
                first = ranking.rank;
            }
        }

        return first;
    }

    private _calculateLevel(lastInRank: number, firstInRank: number, playerRank: number): number {
        let variance = 0.0001;
        let level = (playerRank - lastInRank - variance) / ((firstInRank - lastInRank - variance) / Ranking.MAX_RATING);

        return Math.ceil(level);
    }

    public getSeasonYear(): string {
        const season = this._seasonStore.getSeasonById(this._seasonId);

        return season ? season.seasonYear : ' N/A ';
    }

}

export interface MatchDialogInput {
    seasonMatch: SeasonMatch;
}

export interface CreateMatchDialogInput {
    players: Player[];
    seasonMatch: SeasonMatch;
    mode: string;
    isFinished: boolean;
}

