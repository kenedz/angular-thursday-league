export enum Rights {
    AUTH_USER = 'user',
    AUTH_ADMIN = 'admin'
}
