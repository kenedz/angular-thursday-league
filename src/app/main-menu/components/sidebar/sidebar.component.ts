import {Component, OnInit} from '@angular/core';
import {LeaguesStore, SeasonsStore, TranslateStore} from '../../../core/local-store';
import {League, User} from '../../../core/services';
import {SeasonsService} from "../../../core/services/seasons/seasons.service";
import {Season} from "../../../core/services/seasons/models/Season";
import {MenuItem} from "../menu-item/menu-item.component";
import {Rights} from "./models/Rigts";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    public menuItems: any[];
    public dashboardTitle: string;
    public leaguesTitle: string;
    public seasonsTitle: string;
    public rankingTitle: string;
    public drawTitle: string;
    public userProfileTitle: string;
    public adminConsoleTitle: string;

    private _seasons: Season[];

    user: User;

    constructor(private _translateStore: TranslateStore, private _seasonsService: SeasonsService,
                private _seasonsStore: SeasonsStore, private _leaguesStore: LeaguesStore) {
        _translateStore.onLangChange().subscribe(lang => {
            this._setTranslations();
            window.location.reload();
        });
    }

    async ngOnInit() {
        const league = this._leaguesStore.getAppLeague();
        if (league) {
            this._seasons = await this._seasonsService.getSeasonsByLeagueId(league.leagueId);
            this._seasonsStore.setSeasons(this._seasons);
        }

        this._setTranslations();
        this._initMenuItems(league);
    }

    private _setTranslations(): void {
        this._translateStore.getTranslation('app.menu.dashboardtitle').subscribe(title => this.dashboardTitle = title);
        this._translateStore.getTranslation('app.menu.leaguestitle').subscribe(title => this.leaguesTitle = title);
        this._translateStore.getTranslation('app.menu.seasonstitle').subscribe(title => this.seasonsTitle = title);
        this._translateStore.getTranslation('app.menu.rankingtitle').subscribe(title => this.rankingTitle = title);
        this._translateStore.getTranslation('app.menu.drawtitle').subscribe(title => this.drawTitle = title);
        this._translateStore.getTranslation('app.menu.userprofiletitle').subscribe(title => this.userProfileTitle = title);
        this._translateStore.getTranslation('app.menu.adminconsoletitle').subscribe(title => this.adminConsoleTitle = title);
        this._initSeasonsSubmenuTitles();
    }

    private async _initMenuItems(league: League) {
        this.menuItems = [];

        if (league) {
            this.menuItems.push(
                {
                    path: '/dashboard',
                    title: this.dashboardTitle,
                    icon: 'dashboard',
                    class: '',
                    hasSubmenu: false,
                    auth: Rights.AUTH_USER
                },
                {
                    path: '',
                    title: this.seasonsTitle,
                    icon: 'table_rows',
                    class: '',
                    hasSubmenu: true,
                    submenus: this._getSeasonsSubmenu(this._seasons),
                    auth: Rights.AUTH_USER
                },
                {
                    path: '',
                    title: this.rankingTitle,
                    icon: 'table_chart',
                    class: '',
                    hasSubmenu: true,
                    submenus: this._getRankingsSubmenu(this._seasons),
                    auth: Rights.AUTH_USER
                },
                {
                    path: '/draw',
                    title: this.drawTitle,
                    icon: 'swap_horizontal_circle',
                    class: '',
                    hasSubmenu: false,
                    auth: Rights.AUTH_USER
                }
            );
        }

        this.menuItems.push({
                path: '/user-profile',
                title: this.userProfileTitle,
                icon: 'person',
                class: '',
                hasSubmenu: false,
                auth: Rights.AUTH_USER
            },
            {
                path: '/admin-console',
                title: this.adminConsoleTitle,
                icon: 'lock',
                class: '',
                hasSubmenu: false,
                auth: Rights.AUTH_ADMIN
            });
    }

    private _getSeasonsSubmenu(seasons: Season[]): MenuItem[] {
        const seasonsSubmenu: MenuItem[] = [];
        for (const season of seasons) {
            seasonsSubmenu.push({
                path: '/seasons/' + season.seasonId,
                title: season.seasonYear,
                icon: 'sports_soccer',
                class: '',
                hasSubmenu: false,
                auth: Rights.AUTH_USER
            });
        }

        return seasonsSubmenu;
    }

    private _getRankingsSubmenu(seasons: Season[]): MenuItem[] {
        const rankingsSubmenu: MenuItem[] = [];
        for (const season of seasons) {
            rankingsSubmenu.push({
                path: '/ranking/' + season.seasonId,
                title: season.seasonYear,
                icon: 'sports_soccer',
                class: '',
                hasSubmenu: false,
                auth: Rights.AUTH_USER
            });
        }
        return rankingsSubmenu;
    }

    private _initSeasonsSubmenuTitles() {
        if (this._seasons) {
            for (const season of this._seasons) {
                this._translateStore.getTranslation('app.menu.season_title').subscribe(title => title + ' ' + season.seasonYear);
            }
        }
    }
}
