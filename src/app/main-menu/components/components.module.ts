import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SidebarComponent } from './sidebar/sidebar.component';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {TranslateModule} from '@ngx-translate/core';
import {MenuItemComponent} from "./menu-item/menu-item.component";
import { ToolbarMenuComponent } from './toolbar-menu/toolbar-menu.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatSelectModule} from "@angular/material/select";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatButtonModule,
        MatMenuModule,
        TranslateModule,
        MatToolbarModule,
        MatIconModule,
        MatTooltipModule,
        MatSelectModule,
    ],
  declarations: [
      SidebarComponent,
      MenuItemComponent,
      ToolbarMenuComponent,
  ],
    exports: [
        SidebarComponent,
        ToolbarMenuComponent
    ]
})
export class MainMenuModule { }
