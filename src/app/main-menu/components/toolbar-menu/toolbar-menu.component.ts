import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "angularx-social-login";
import {ILanguage, LeaguesStore, LoginStore, TranslateStore} from "../../../core/local-store";
import {TranslateService} from "@ngx-translate/core";
import {League} from "../../../core/services";
// direct import to avoid circular dependency
import {LeaguesService} from "../../../core/services/leagues/leagues.service";
import {SidebarComponent} from "../sidebar/sidebar.component";
import {Router} from "@angular/router";

@Component({
    selector: 'app-toolbar-menu',
    templateUrl: './toolbar-menu.component.html',
    styleUrls: ['./toolbar-menu.component.scss']
})
export class ToolbarMenuComponent implements OnInit {

    @ViewChild(SidebarComponent, {static: true}) sidebar: SidebarComponent;

    public languages: ILanguage[];
    public selectedLanguage: ILanguage;

    private _leagues: League[];
    public selectedLeague: League;

    constructor(private _authService: AuthService, private _loginStore: LoginStore, private _translationStore: TranslateStore,
                public translateService: TranslateService, private _leaguesService: LeaguesService, private _leagesStore: LeaguesStore,
                private _router: Router) {
    }

    public ngOnInit() {
        this.languages = this._translationStore.getLanguages();
        this._initLeagues();
        this.selectedLeague = this._leagesStore.getAppLeague();
    }

    private _initLeagues(): void {
        this._leaguesService.getLeagues().then(leagues => {
            this._leagues = leagues;
            this._leagues.push({leagueName: "", leagueId: undefined, country: undefined, seasons: undefined, sport: undefined});
        });
    }

    public onLanguageSelection() {
        console.log("language: " + this.selectedLanguage);
        this._translationStore.storeLanguage(this.selectedLanguage);
    }

    signOut(): void {
        this._authService.signOut().then(() => {
            this._loginStore.removeStorage();
        });
    }

    public onLeaguesSelectionChange(league: League): void {
        this.selectedLeague = league;
        this._leagesStore.storeAppLeague(league);
        this.sidebar.ngOnInit();
        if (this.selectedLeague.leagueName.length > 0) {
            this._router.navigateByUrl('dashboard');
        } else {
            this._router.navigateByUrl('home');
        }
    }

    public getLeagues(): League[] {
        return this._leagues;
    }

    public getAppTitle(): string {
        return this.selectedLeague ? this.selectedLeague.leagueName : "";
    }

    public getHref(): string {
        if (this.selectedLeague && this.selectedLeague.leagueName.length > 0) {
            return "#/dashboard";
        }
        return "";
    }
}
