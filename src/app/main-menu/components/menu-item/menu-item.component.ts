import {Component, Input, OnInit} from '@angular/core';
import {LoginStore} from "../../../core/local-store";
import {Rights} from "../sidebar/models/Rigts";


export interface MenuItem {
    path: string;
    title: string;
    icon: string;
    class: string;
    hasSubmenu: boolean;
    submenus?: MenuItem[];
    auth: string;
}

@Component({
    selector: 'app-menu-item',
    templateUrl: './menu-item.component.html',
    styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

    @Input()
    public menuItem: MenuItem;

    @Input()
    public isSubmenu = false;

    constructor(private _loginService: LoginStore) {
    }

    public ngOnInit() {
    }

    public menuVisible(menuItem: MenuItem): boolean {
        const menuVisible = true;
        if (menuItem.auth === Rights.AUTH_USER) {
            return menuVisible;
        }

        // menu is admin visible
        if (this._loginService.isCurrentUserAdmin()) {
            return menuVisible;
        }

        return !menuVisible;
    }


}
