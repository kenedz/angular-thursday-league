import { Routes } from '@angular/router';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import {SeasonsComponent} from '../seasons/seasons.component';
import {RankingComponent} from '../ranking/ranking.component';
import {DrawComponent} from '../draw/draw.component';
import {AdminConsoleComponent} from '../admin-console/admin-console.component';
import {AuthGuard} from '../core/AuthGuard';
import {HomeComponent} from "../home/home.component";

export const MainMenuRoutes: Routes = [
    { path: 'home',            component: HomeComponent },
    { path: 'dashboard',       component: DashboardComponent },
    { path: 'seasons/:season', component: SeasonsComponent, runGuardsAndResolvers: 'always', canActivate: [AuthGuard]},
    { path: 'ranking/:season', component: RankingComponent, runGuardsAndResolvers: 'always', canActivate: [AuthGuard]},
    { path: 'draw',            component: DrawComponent },
    { path: 'user-profile',    component: UserProfileComponent },
    { path: 'admin-console',   component: AdminConsoleComponent }
];
