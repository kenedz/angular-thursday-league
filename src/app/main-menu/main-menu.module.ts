import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MainMenuRoutes} from './main-menu.routing';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {UserProfileComponent} from '../user-profile/user-profile.component';

import {MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatSelectModule, MatTooltipModule} from '@angular/material';
import {SeasonsComponent} from '../seasons/seasons.component';
import {RankingComponent} from '../ranking/ranking.component';
import {DrawComponent} from '../draw/draw.component';
import {AdminConsoleComponent} from '../admin-console/admin-console.component';
import {MatMenuModule} from '@angular/material/menu';
import {TranslateModule} from '@ngx-translate/core';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import {UserDialogComponent} from '../admin-console/components/user-dialog/user-dialog.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatchDetailDialogComponent} from '../seasons/components/match-detail-dialog/match-detail-dialog.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatchDialogComponent} from '../seasons/components/match-dialog/match-dialog.component';
import {MatStepperModule} from '@angular/material/stepper';
import {CalendarModule, InputTextModule, ListboxModule, RatingModule} from 'primeng';
import {UserConsoleComponent} from "../admin-console/components/user-console/user-console.component";
import {SeasonConsoleComponent} from "../admin-console/components/season-console/season-console.component";
import {SeasonDialogComponent} from "../admin-console/components/season-dialog/season-dialog.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule} from "@angular-material-components/datetime-picker";
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";
import {DrawComponentsModule} from "../draw/components/draw-components.module";
import {HomeComponent} from "../home/home.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MainMenuRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatMenuModule,
        TranslateModule,
        MatTableModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatPaginatorModule,
        MatSortModule,
        MatGridListModule,
        MatStepperModule,
        CalendarModule,
        ListboxModule,
        InputTextModule,
        RatingModule,
        MatDatepickerModule,
        MatNativeDateModule,
        NgxMatDatetimePickerModule,
        NgxMatTimepickerModule,
        NgxMatNativeDateModule,
        MatToolbarModule,
        MatIconModule,
        MatDividerModule,
        DrawComponentsModule,
    ],
    declarations: [
        DashboardComponent,
        SeasonsComponent,
        RankingComponent,
        DrawComponent,
        UserProfileComponent,
        AdminConsoleComponent,
        UserDialogComponent,
        MatchDetailDialogComponent,
        MatchDialogComponent,
        UserConsoleComponent,
        SeasonConsoleComponent,
        SeasonDialogComponent,
        HomeComponent
    ],
    entryComponents: [
        UserDialogComponent,
        MatchDetailDialogComponent,
        MatchDialogComponent,
        SeasonDialogComponent,
    ]
})

export class MainMenuModule {
}
