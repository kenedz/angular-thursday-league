export class AppSettings {
    public static API_ENDPOINT = 'http://localhost:8080/server';
    public static readonly CURRENT_SEASON = '2019-2020';
}
