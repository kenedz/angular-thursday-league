import {Inject, Injectable} from "@angular/core";
import {LOCAL_STORAGE, StorageService} from "ngx-webstorage-service";
import {Season} from "../services/seasons/models/Season";
import {AppSettings} from "../AppSettings";

const SEASONS = 'seasons';

@Injectable()
export class SeasonsStore {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    }

    public setSeasons(seasons: Season[]): void {
        this.storage.set(SEASONS, seasons);
    }

    public getSeasons(): Season[] {
        return this.storage.get(SEASONS);
    }

    public getSeasonById(seasonId: number): Season {

        const seasons = this.getSeasons();
        const filteredSeasons = seasons.filter(season => season.seasonId === seasonId);

        if (filteredSeasons.length === 0) {
            console.debug("SeasonId not found!");
        } else if (filteredSeasons.length > 1) {
            console.debug("SeasonId is not unique!");
        }

        return filteredSeasons[0];
    }

    public getSeasonsByLeagueId(leagueId: number): Season[] {
        const seasons = this.getSeasons();
        return seasons.filter(season => season.leagueId === leagueId);
    }

    public getCurrentSeasonByLeagueId(leagueId: number): Season | undefined {
        const seasons: Season[] = this.getSeasonsByLeagueId(leagueId).filter(season => season.seasonYear === AppSettings.CURRENT_SEASON);
        if (seasons.length === 1) {
            return seasons[0];
        }
        return undefined;
    }
}
