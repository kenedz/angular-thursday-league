import {EventEmitter, Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {Observable} from "rxjs";

const STORAGE_KEY = 'translate';

export interface ILanguage {
    language: string;
    imgPath: string;
}

@Injectable()
export class TranslateStore {

    public EN_LANG = 'en';
    public CZ_LANG = 'cz';

    private _languages: ILanguage[];

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, public translate: TranslateService) {
        this.setDefaults();
    }

    public setDefaults() {
        const _czLang: ILanguage = {language: this.CZ_LANG, imgPath: "assets/img/flags/czech_republic_flag.png"};
        const _enLang: ILanguage = {language: this.EN_LANG, imgPath: "assets/img/flags/united_kingdom_flag.png"};

        this._languages = [_czLang, _enLang];

        this.translate.addLangs([_czLang.language, _enLang.language]);

        if (!this.getLanguage()) {
            this.storeLanguage(_enLang);
        } else {
            this._setTranslationLanguage(this.getLanguage().language);
        }
    }

    public getLanguage(): ILanguage {
        return this.storage.get(STORAGE_KEY);
    }

    public getLanguages(): ILanguage[] {
        return this._languages;
    }

    public storeLanguage(language: ILanguage) {
        this.storage.set(STORAGE_KEY, language);
        this._setTranslationLanguage(language.language);
    }

    private _setTranslationLanguage(language: string) {
        this.translate.use(language);
    }

    public getTranslation(xmlPath: string): Observable<string | any> {
        return this.translate.get(xmlPath);
    }

    public onLangChange(): EventEmitter<LangChangeEvent> {
        return this.translate.onLangChange;
    }

}
