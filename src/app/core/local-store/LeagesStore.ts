import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';
import {League} from "../services";

const APP_LEAGUE = 'appleague';

@Injectable()
export class LeaguesStore {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    }

    public getAppLeague(): League | undefined {
        const league: League = this.storage.get(APP_LEAGUE);
        return league && league.leagueId ? league : undefined;
    }

    public storeAppLeague(league: League): void {
        this.storage.set(APP_LEAGUE, league);
    }

    public removeStorage() {
        this.storage.remove(APP_LEAGUE);
    }

}
