import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';

const REFRESH_RANKINGS = 'refreshrankings';

@Injectable()
export class RankingsStore {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    }

    public refreshRankings(): boolean {
        return this.storage.get(REFRESH_RANKINGS);
    }

    public storeRefreshRankings(refreshRankings: boolean): void {
        this.storage.set(REFRESH_RANKINGS, refreshRankings);
    }

    public removeStorage() {
        this.storage.remove(REFRESH_RANKINGS);
    }

}
