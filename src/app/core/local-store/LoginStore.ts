import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';
import {SocialUser} from 'angularx-social-login';
import {UsersService} from '../services/users/users.service';
import {User} from '../services/users/models/User';
import {Role} from '../services/roles/models/Role';

const SOCIAL_USER_STORAGE_KEY = 'login';
const APP_USER_STORAGE_KEY = 'app-login';

@Injectable()
export class LoginStore {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, private userService: UsersService) {
    }

    public getSocialLogin(): SocialUser {
        return this.storage.get(SOCIAL_USER_STORAGE_KEY);
    }

    public getAppUser(): User {
        return this.storage.get(APP_USER_STORAGE_KEY);
    }

    public storeLogin(user: SocialUser): void {
        if (!user) {
            this.storage.set(SOCIAL_USER_STORAGE_KEY, user);
        } else {
            this.storage.set(SOCIAL_USER_STORAGE_KEY, JSON.parse(JSON.stringify(user)));

            this.userService.getUserByEmail(user.email).then(user => {
                this.storage.set(APP_USER_STORAGE_KEY, user);
            });
        }
    }

    public removeStorage() {
        this.storage.remove(SOCIAL_USER_STORAGE_KEY);
        this.storage.remove(APP_USER_STORAGE_KEY);
    }

    public signedOut(): boolean {
        return !this.getSocialLogin();
    }

    public isCurrentUserAdmin() {
        let user = this.getAppUser();
        if (!user) {
            return false;
        }

        let role = user.role;

        return role.role === Role.SUPERADMIN || role.role === Role.ADMIN;
    }
}
