import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginStore} from './local-store/LoginStore';

@Injectable()
export class AuthGuard implements CanActivate {


    constructor(protected router: Router, private loginService: LoginStore) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

        if (this.loginService.signedOut()) {
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});

            return false;
        }

        return true;
    }

}
