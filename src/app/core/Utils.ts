import {Season} from "./services/seasons/models/Season";
import {AppSettings} from "./AppSettings";
import {Player, Ranking} from "./services";

export class Utils {
    public static getCurrentSeason(seasons: Season[]): Season | null {
        for(const season of seasons) {
            if (season.seasonYear === AppSettings.CURRENT_SEASON) {
                return season;
            }
        }

        return null;
    }

    public static sortRankings(rankings: Ranking[]) {
        return rankings.sort((n1, n2) => {
            if (n2.pointsAvg === n1.pointsAvg) {
                let n2Difference = n2.scoredOverall - n2.receivedOverall;
                let n1Difference = n1.scoredOverall - n1.receivedOverall;
                return n2Difference - n1Difference;
            }

            return n2.pointsAvg - n1.pointsAvg;
        });
    }

    public static isNumeric(val: any): boolean {
        return !(val instanceof Array) && (val - parseFloat(val) + 1) >= 0;
    }

    public static sortPlayers(players: Player[]) {
        players.sort((a, b) => {
            const playerAStr = a.lastName + ' ' + a.firstName + ' (' + a.email + ')';
            const playerBStr = b.lastName + ' ' + b.firstName + ' (' + b.email + ')'

            return this._compareByName(playerAStr, playerBStr, true);
        });
    }

    private static _compareByName(a: string, b: string, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
}
