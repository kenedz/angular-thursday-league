import {Sport} from "../../sports/models/Sport";
import {Country} from "../../countries/models/Country";
import {Season} from "../../seasons/models/Season";

export interface League {
    leagueId: number;
    leagueName: string;
    sport: Sport;
    country: Country;
    seasons: Season[];
}
