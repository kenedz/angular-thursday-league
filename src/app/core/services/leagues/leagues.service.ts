import {Injectable} from '@angular/core';
import {League} from "./models/League";
import {BaseService} from "../BaseService";

@Injectable({
    providedIn: 'root'
})
export class LeaguesService extends BaseService<League> {

    private serviceSubUrl = '/leagues';

    public async getLeagues(): Promise<League[]> {
        return this._get(this.serviceSubUrl);
    }
}
