import {Injectable} from '@angular/core';
import {Match, Season} from '../';
import {Utils} from "../..";
import {SeasonsStore} from "../../local-store";
import {BaseService} from "../BaseService";
import {AppModule} from "../../../app.module";

@Injectable({
    providedIn: 'root'
})
export class MatchesService extends BaseService<Match> {

    private _serviceSubUrl = '/matches';
    private _seasonsStore: SeasonsStore;

    ngOnInit(): void {
        this._seasonsStore = AppModule.injector.get(SeasonsStore);
    }

    public getMatchesBySeasonId(seasonId: number): Promise<Match[]> {
        return this._get(this._serviceSubUrl + '/filter/' + seasonId);
    }

    public getCurrentSeasonMatchesByLeagueId(leagueId: number): Promise<Match[]> {
        const currentSeason: Season = Utils.getCurrentSeason(this._seasonsStore.getSeasonsByLeagueId(leagueId));
        return this._get(this._serviceSubUrl + '/filter/' + currentSeason.seasonId);
    }

    public createMatch(match: Match): Promise<Match> {
        return this._post(this._serviceSubUrl, match);
    }

    public updateMatch(match: Match): Promise<Match> {
        return this._put(this._serviceSubUrl, match);
    }

    public deleteMatch(id: string): Promise<Match> {
        return this._delete(this._serviceSubUrl + '/' + id);
    }

}
