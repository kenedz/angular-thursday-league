import {Lineup} from '../../matches-lineup/models/Lineup';
import {Season} from "../../seasons/models/Season";

export class Match {
    static readonly POINTS_PER_WIN = 2;
    static readonly POINTS_PER_DRAW = 1;

    matchId: number;
    matchDate: Date;
    homeScored: number;
    awayScored: number;
    lineup: Lineup[];
    season: Season;
    seasonId: number;
}
