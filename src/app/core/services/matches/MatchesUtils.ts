import {OnInit} from "@angular/core";
import {Match} from "./models/Match";
import {Lineup} from "../matches-lineup/models/Lineup";
import {SeasonMatch} from "../../../seasons/models/SeasonMatch";

export class MatchesUtils {

    constructor() {
    }

    ngOnInit(): void {
    }

    public static convertToSeasonMatches(matches: Match[]): SeasonMatch[] {
        let seasonMatches = new Array<SeasonMatch>();
        let index = 1;

        matches.forEach(match => {
            seasonMatches.push(MatchesUtils.convertToSeasonMatch(match, index++));
        });

        return seasonMatches;
    }

    public static convertToSeasonMatch(match: Match, index: number): SeasonMatch {
        let seasonMatch = new SeasonMatch();
        seasonMatch.id = index;
        seasonMatch.match = match;
        return seasonMatch;
    }

    public static sortMatchesByDate(matches: Match[], asc: boolean = true): Match[] {
        const data = matches.slice();
        return data.sort((a, b) => {
            return this.compareByDate(a.matchDate, b.matchDate, asc);
        });

    }

    public static compareByDate(a: Date, b: Date, isAsc: boolean) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

}

