import {Injectable} from '@angular/core';
import {Role} from './models/Role';
import {BaseService} from "../BaseService";

@Injectable({
    providedIn: 'root'
})
export class RolesService extends BaseService<Role> {

    private serviceSubUrl = '/roles';

    public getRoles() {
        return this._get(this.serviceSubUrl);
    }
}
