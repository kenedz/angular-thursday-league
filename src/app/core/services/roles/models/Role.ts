export class Role {
    public static readonly SUPERADMIN = 'superadmin';
    public static readonly ADMIN = 'admin';
    public static readonly USER = 'user';

    roleId: number;
    role: string;
    description: string;
}
