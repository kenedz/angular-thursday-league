import {Player} from '../../players/models/Player';
import {Season} from "../../seasons/models/Season";

export class Ranking {
    static readonly MAX_RATING = 10;

    rankingId: number;
    rank: number;
    pointsAvg: number;
    level: number;
    pointsOverall: number;
    played: number;
    missed: number;
    wins: number;
    draws: number;
    loses: number;
    scoredOverall: number;
    scoredAvg: number;
    receivedOverall: number;
    receivedAvg: number;
    player: Player;
    season: Season;
    seasonId: number;
}
