import {Injectable} from '@angular/core';
import {Ranking} from './models/Ranking';
import {SeasonsStore} from "../../local-store";
import {Utils} from "../../Utils";
import {Season} from "../seasons/models/Season";
import {BaseService} from "../BaseService";
import {AppModule} from "../../../app.module";

@Injectable({
    providedIn: 'root'
})
export class RankingService extends BaseService<Ranking> {

    private serviceSubUrl = '/ranking';
    private rankingsUrl = this.serviceSubUrl + '/rankings';

    private _seasonsStore: SeasonsStore;

    public ngOnInit(): void {
        this._seasonsStore = AppModule.injector.get(SeasonsStore);
    }

    public getRankingsBySeasonId(seasonId: number): Promise<Ranking[]> {
        return this._get(this.rankingsUrl + '/' + seasonId);
    }

    public getCurrentRankingsByLeagueId(leagueId: number): Promise<Ranking[]> {
        const currentSeason: Season = Utils.getCurrentSeason(this._seasonsStore.getSeasonsByLeagueId(leagueId));
        if (!currentSeason) {
            return null;
        }

        return this._get(this.rankingsUrl + '/' + currentSeason.seasonId);
    }

    public createRankings(rankings: Ranking[]) {
        return this._postAll(this.rankingsUrl, rankings);
    }

}
