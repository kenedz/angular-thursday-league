import {Injectable, OnInit} from "@angular/core";
import {AppSettings} from "../AppSettings";
import {MainClient} from "../MainClient";

@Injectable()
export class BaseService<T> implements OnInit {

    private serviceUrl = AppSettings.API_ENDPOINT;

    constructor(private _mainClient: MainClient<T>) {
        this.ngOnInit();
    }

    public ngOnInit(): void {
    }

    protected _get(serviceSubUrl: string): Promise<T[]> {
        return this._mainClient.get(this.serviceUrl + serviceSubUrl);
    }

    protected _post(serviceSubUrl: string, o: T): Promise<T> {
        return this._mainClient.post(this.serviceUrl + serviceSubUrl, o);
    }

    protected _postAll(serviceSubUrl: string, o: T[]): Promise<T[]> {
        return this._mainClient.postAll(this.serviceUrl + serviceSubUrl, o);
    }

    protected _put(serviceSubUrl: string, o: T): Promise<T> {
        return this._mainClient.put(this.serviceUrl + serviceSubUrl, o);
    }

    protected _delete(serviceSubUrl: string): Promise<T> {
        return this._mainClient.delete(this.serviceUrl + serviceSubUrl);
    }

}
