import {Injectable} from '@angular/core';
import {User} from './models/User';
import {BaseService} from "../BaseService";

@Injectable({
    providedIn: 'root'
})
export class UsersService extends BaseService<User> {

    private serviceSubUrl = '/users';
    private filterUrl = this.serviceSubUrl + '/filter/';

    public getUsers() {
        return this._get(this.serviceSubUrl);
    }

    public getUserByEmail(email: string) {
        return this._get(this.filterUrl + email).then(data => {
            if (!data[0]) {
                return null;
            }
            return data[0];
        });
    }

    public createUser(user: User) {
        return this._post(this.serviceSubUrl, user);
    }

    public updateUser(user: User) {
        return this._put(this.serviceSubUrl, user);
    }

    public deleteUser(id: string) {
        return this._delete(this.serviceSubUrl + '/' + id);
    }

}
