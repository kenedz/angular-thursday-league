import {Role} from '../../roles/models/Role';

export class User {
    userId: number;
    email: string;
    firstName: string;
    lastName: string;
    role: Role;
}
