import {Injectable} from '@angular/core';
import {Player} from './models/Player';
import {BaseService} from "../BaseService";

@Injectable({
    providedIn: 'root'
})
export class PlayersService extends BaseService<Player> {

    private serviceSubUrl = '/players';

    public getPlayers(): Promise<Player[]> {
        return this._get(this.serviceSubUrl);
    }

}
