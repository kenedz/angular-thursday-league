export class Player {
    playerId: number;
    email: string;
    firstName: string;
    lastName: string;
}
