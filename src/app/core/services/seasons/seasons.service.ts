import {Injectable} from '@angular/core';
import {Season} from "./models/Season";
import {BaseService} from "../BaseService";

@Injectable({
    providedIn: 'root'
})
export class SeasonsService extends BaseService<Season> {

    private serviceSubUrl = '/seasons';

    public getSeasons(): Promise<Season[]> {
        return this._get(this.serviceSubUrl);
    }

    public getSeasonsByLeagueId(leagueId: number): Promise<Season[]> {
        return this._get(this.serviceSubUrl + "/league/" + leagueId);
    }

    public createSeason(season: Season): Promise<Season> {
        return this._post(this.serviceSubUrl, season);
    }

    public updateSeason(season: Season): Promise<Season> {
        return this._put(this.serviceSubUrl, season);
    }

    public deleteSeason(seasonId: string): Promise<Season> {
        return this._delete(this.serviceSubUrl + '/' + seasonId);
    }
}
