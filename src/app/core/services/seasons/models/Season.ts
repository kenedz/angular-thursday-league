import {Match} from "../../matches/models/Match";
import {Ranking} from "../../rankings/models/Ranking";
import {League} from "../../leagues/models/League";

export class Season {
    seasonId: number;
    seasonYear: string;
    seasonStart: Date;
    seasonEnd: Date;
    league: League;
    leagueId: number;
    matches: Match[];
    rankings: Ranking[];
}
