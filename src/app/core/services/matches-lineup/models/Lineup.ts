import {Match} from '../../matches/models/Match';
import {User} from '../../users/models/User';
import {Player} from '../../players/models/Player';

export class Lineup {
    public static readonly HOME_TEAM = 'home';
    public static readonly AWAY_TEAM = 'away';

    lineupId: number;
    matchId: number;
    match: Match;
    player: Player;
    team: string;
}
