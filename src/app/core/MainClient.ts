import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class MainClient<T> {

    constructor(private _httpClient: HttpClient) {
    }

    public get(serviceUrl: string): Promise<T[]> {
        return this._httpClient.get(serviceUrl).toPromise().then(res => <T[]>res);
    }

    public post(serviceUrl: string, o: T): Promise<T> {
        return this._httpClient.post<T>(serviceUrl, o, {observe: 'response'}).toPromise().then(res => <T>(res.body));
    }

    public postAll(serviceUrl: string, o: T[]): Promise<T[]> {
        return this._httpClient.post(serviceUrl, o, {observe: 'response'}).toPromise().then(res => <T[]>(res.body));
    }

    public put(serviceUrl: string, o: T): Promise<T> {
        return this._httpClient.put(serviceUrl, o, {observe: 'response'}).toPromise().then(res => <T>(res.body));
    }

    public delete(serviceUrl: string): Promise<T> {
        return this._httpClient.delete(serviceUrl, {observe: 'response'}).toPromise().then(res => <T>res.body);
    }

}
