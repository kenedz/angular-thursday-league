import {Player, Ranking} from "../core/services";

export class TeamDistribution {
    homeTeam: Player[];
    awayTeam: Player[];

    constructor() {
        this.homeTeam = [];
        this.awayTeam = [];
    }

}

export interface IDrawCalculator {
    drawCalculation(rankings: Ranking[]): TeamDistribution;
}
