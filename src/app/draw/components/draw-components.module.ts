import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrawStepperComponent } from './draw-stepper/draw-stepper.component';
import {MatStepperModule} from "@angular/material/stepper";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {ListboxModule} from "primeng";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {NgxMatDatetimePickerModule} from "@angular-material-components/datetime-picker";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatSnackBarModule} from "@angular/material/snack-bar";

@NgModule({
    declarations: [DrawStepperComponent],
    exports: [
        DrawStepperComponent
    ],
    imports: [
        CommonModule,
        MatStepperModule,
        TranslateModule,
        ReactiveFormsModule,
        MatButtonModule,
        ListboxModule,
        FormsModule,
        MatCheckboxModule,
        MatDividerModule,
        MatFormFieldModule,
        MatDatepickerModule,
        NgxMatDatetimePickerModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule
    ]
})
export class DrawComponentsModule { }
