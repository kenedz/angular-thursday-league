import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawStepperComponent } from './draw-stepper.component';

describe('DrawStepperComponent', () => {
  let component: DrawStepperComponent;
  let fixture: ComponentFixture<DrawStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
