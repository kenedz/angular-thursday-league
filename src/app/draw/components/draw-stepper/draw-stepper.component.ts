import {Component, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {FormControl, FormGroup} from "@angular/forms";
import {SelectItem} from "primeng";
import {League, Lineup, Match, MatchesService, Player, PlayersService, Ranking, RankingService, Season} from "../../../core/services";
import {IDrawCalculator, TeamDistribution} from "../../IDrawCalculator";
import {DrawCalculator} from "../../DrawCalculator";
import {LeaguesStore, SeasonsStore, TranslateStore} from "../../../core/local-store";
import {MatchDetailDialogComponent} from "../../../seasons/components/match-detail-dialog/match-detail-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {MatchesUtils} from "../../../core/services/matches/MatchesUtils";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-draw-stepper',
    templateUrl: './draw-stepper.component.html',
    styleUrls: ['./draw-stepper.component.scss']
})
export class DrawStepperComponent implements OnInit {

    public matchFormGroup: FormGroup;

    public players: SelectItem[];
    public selectedPlayers: SelectItem[] = [];

    public createMatch = false;

    public matchDate: Date = new Date();

    private _drawCalculator: IDrawCalculator;
    public league: League;

    // translation
    private _yes: string;
    private _no: string;
    private _matchCreated: string;

    constructor(public translateService: TranslateService, private _playersService: PlayersService, private _rankingService: RankingService,
                private _matchService: MatchesService, private _seasonsStore: SeasonsStore, private _leaguesStore: LeaguesStore,
                public dialog: MatDialog, private _translateStore: TranslateStore, private _snackBar: MatSnackBar) {
        this.matchFormGroup = new FormGroup({
            matchDateTimeControl: new FormControl({value: ''}),
        });

        this._drawCalculator = new DrawCalculator();
    }

    public async ngOnInit() {
        const players: Player[] = await this._playersService.getPlayers();
        this.players = this._createListboxPlayers(players);
        this.league = this._leaguesStore.getAppLeague();
        this._setTranslations();
    }

    private _setTranslations() {
        this._translateStore.getTranslation('yes').subscribe(translation => this._yes = translation);
        this._translateStore.getTranslation('no').subscribe(translation => this._no = translation);
        this._translateStore.getTranslation('page.draw.stepper.completion.matchcreated').subscribe(translation => this._matchCreated = translation);
    }

    private _createListboxPlayers(players: Player[]): SelectItem[] {
        let listboxPlayers = new Array<SelectItem>();

        players.forEach(player => {
            listboxPlayers.push(this._getListboxPlayer(player));
        });

        return listboxPlayers.sort((a, b) => {
            return this.compareByLabel(a.label, b.label, true);
        });
    }

    private _getListboxPlayer(player: Player): SelectItem {
        return {label: this._getLabel(player), value: player};
    }

    private _getLabel(player: Player): string {
        return player.lastName + ' ' + player.firstName + ' (' + player.email + ')';
    }

    private compareByLabel(a: string, b: string, isAsc: boolean): number {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    public onCancelClick(): void {

    }

    public async draw(): Promise<void> {
        if (this.league) {
            const selectedPlayersRankings = this._getSelectedPlayersRanking(await this._rankingService.getCurrentRankingsByLeagueId(this.league.leagueId));

            const teamDistribution: TeamDistribution = this._drawCalculator.drawCalculation(selectedPlayersRankings);

            const match: Match = this._createMatch(teamDistribution);
            let createdMatch: Match;

            if (this.createMatch) {
                createdMatch = await this._matchService.createMatch(match);

                const currentSeason = this._seasonsStore.getSeasonById(match.seasonId);
                let currentSeasonYear: string;
                if (currentSeason) {
                    currentSeasonYear = currentSeason.seasonYear;
                }
                this._openSnackBar(this._matchCreated + ' ' + currentSeasonYear + '.', "");
            }

            this._openDrawedMatch(createdMatch? createdMatch : match);
        }
    }

    private _openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 8000,
        });
    }

    private _openDrawedMatch(match: Match) {
        this.dialog.open(MatchDetailDialogComponent, {
            data: {seasonMatch: MatchesUtils.convertToSeasonMatch(match, 0)}
        });
    }

    public drawBtnVisible(): boolean {
        return this.selectedPlayers && this.selectedPlayers.length > 1;
    }

    public get matchDateTimeControl() {
        return this.matchFormGroup.get('matchDateTimeControl');
    }

    public getLineup(): string {
        let lineup = '';
        const delimiter = ', ';

        this.selectedPlayers.forEach(player => {
            const p: Player = player.value;
            lineup = lineup + p.firstName + ' ' + p.lastName + delimiter;
        });

        return lineup.length > 0 ? lineup.substring(0, lineup.lastIndexOf(delimiter)) : ' - ';
    }

    private _getSelectedPlayersRanking(rankings: Ranking[]): Ranking[] {
        const rankingsMap: Map<number, Ranking> = new Map<number, Ranking>();
        const selectedPlayersRanking: Ranking[] = [];
        rankings.forEach(ranking => {
            rankingsMap.set(ranking.player.playerId, ranking);
        });

        this.selectedPlayers.forEach(player => {
            const playerId: number = (player.value as Player).playerId;
            if (rankingsMap.has(playerId)) {
                selectedPlayersRanking.push(rankingsMap.get(playerId));
            }
        });

        return selectedPlayersRanking;
    }

    private _createMatch(teamDistribution: TeamDistribution): Match {
        const match: Match = new Match();
        match.matchDate = this.matchDate;
        match.lineup = this._getLineupFromTeamDistribution(teamDistribution);

        const season: Season | undefined = this._seasonsStore.getCurrentSeasonByLeagueId(this.league.leagueId);
        match.seasonId = season ? season.seasonId : undefined;
        match.season = season;

        return match;
    }

    private _getLineupFromTeamDistribution(teamDistribution: TeamDistribution): Lineup[] {
        const lineup: Lineup[] = [];
        if (teamDistribution) {
            teamDistribution.homeTeam.forEach(player => {
                lineup.push({lineupId: undefined, match: undefined, matchId: undefined, player, team: Lineup.HOME_TEAM});
            });
            teamDistribution.awayTeam.forEach(player => {
                lineup.push({lineupId: undefined, match: undefined, matchId: undefined, player, team: Lineup.AWAY_TEAM});
            });
        }

        return lineup;
    }

    public getCreateMatchLabel(): string {
        console.log(this._yes + " " +  this._no);
        return this.createMatch ? this._yes : this._no;
    }

}
