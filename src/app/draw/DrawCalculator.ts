import {IDrawCalculator, TeamDistribution} from "./IDrawCalculator";
import {Ranking} from "../core/services";
import {Utils} from "../core";

export class DrawCalculator implements IDrawCalculator {

    public drawCalculation(rankings: Ranking[]): TeamDistribution {
        if (!rankings) {
            return undefined;
        }

        const teamDistribution: TeamDistribution = new TeamDistribution();

        // sort by level
        Utils.sortRankings(rankings);

        let pushToHomeTeam = false;
        // the best players goes to home team, next two best to the away team, next two best to the home team etc.
        for (let i: number = 0; i < rankings.length; i++) {
            if (i === 0) {
                teamDistribution.homeTeam.push(rankings[i].player);
                pushToHomeTeam = false;
                continue;
            }

            if (pushToHomeTeam) {
                teamDistribution.homeTeam.push(rankings[i].player);
                if (i + 1 < rankings.length) {
                    i++;
                    teamDistribution.homeTeam.push(rankings[i].player);
                }
                pushToHomeTeam = false;
            } else {
                teamDistribution.awayTeam.push(rankings[i].player);
                if (i + 1 < rankings.length) {
                    i++;
                    teamDistribution.awayTeam.push(rankings[i].player);
                }
                pushToHomeTeam = true;
            }
        }

        return teamDistribution;
    }

}
