import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Injector, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {AppRoutingModule} from './app.routing';
import {MainMenuModule} from './main-menu/components/components.module';

import {AppComponent} from './app.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {AuthServiceConfig, GoogleLoginProvider, LoginOpt, SocialLoginModule} from 'angularx-social-login';
import {LoginStore, RankingsStore, SeasonsStore, TranslateStore, LeaguesStore} from './core/local-store';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './core/AuthGuard';
import {MainMenuComponent} from "./main-menu/main-menu.component";
import {MainClient} from "./core/MainClient";
import {BaseService} from "./core/services/BaseService";
import { HomeComponent } from './home/home.component';

const googleLoginOptions: LoginOpt = {
    scope: 'profile email'
};

const config = new AuthServiceConfig([
    {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('920939348796-ikatfrhvkhkgo6kfra4ntd28d2huh8f1.apps.googleusercontent.com', googleLoginOptions)
    }
]);

export function provideConfig() {
    return config;
}

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '-lang.json');
}

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MainMenuModule,
        RouterModule,
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        SocialLoginModule,
        HttpClientModule,
    ],
    declarations: [
        AppComponent,
        MainMenuComponent,
        LoginComponent,
    ],
    providers: [
        {
            provide: AuthServiceConfig,
            useFactory: provideConfig
        },
        TranslateStore,
        LoginStore,
        RankingsStore,
        SeasonsStore,
        AuthGuard,
        MainClient,
        BaseService,
        LeaguesStore
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    /**
     * Allows for retrieving singletons using `AppModule.injector.get(MyService)`
     * This is good to prevent injecting the service as constructor parameter.
     */
    static injector: Injector;

    constructor(injector: Injector) {
        AppModule.injector = injector;
    }
}
