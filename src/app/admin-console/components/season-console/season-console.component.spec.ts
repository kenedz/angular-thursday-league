import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonConsoleComponent } from './season-console.component';

describe('SeasonConsoleComponent', () => {
  let component: SeasonConsoleComponent;
  let fixture: ComponentFixture<SeasonConsoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonConsoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonConsoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
