import {Component, OnInit, ViewChild} from '@angular/core';
import {League, SeasonsService, LeaguesService, Season} from "../../../core/services";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {MatDialog} from "@angular/material/dialog";
import {SeasonDialogComponent} from "../season-dialog/season-dialog.component";
import {DialogOperations} from "../../models/DialogOperations";

@Component({
  selector: 'app-season-console',
  templateUrl: './season-console.component.html',
  styleUrls: ['./season-console.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class SeasonConsoleComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  private _leagues: League[];
  private _lastOperation: string;

  public selectedLeague: League;

  private _newSeason: Season;
  public seasonDataSource: MatTableDataSource<Season>;
  public seasonTableColumns: string[] = ['name'];
  public selectedLeagueOption = '';
  public expandedSeasonElement: Season | null;

  constructor(private _seasonService: SeasonsService, private _leaguesService: LeaguesService, public dialog: MatDialog) { }

  ngOnInit() {
    this._initLeagues();
  }

  private _initLeagues(): void {
    this._leaguesService.getLeagues().then(leagues => {
      this._leagues = leagues;
    });
  }

  private _updateSeasonDataSource(leagueId: number): void {
    this._seasonService.getSeasonsByLeagueId(leagueId).then(seasons => {
      if (!this.seasonDataSource) {
        this.seasonDataSource = new MatTableDataSource<Season>(seasons);
        this.seasonDataSource.paginator = this.paginator;
      } else {
        this.seasonDataSource.data = seasons;
      }
    });
  }

  public updateSeason(season: Season): void {
    this._lastOperation = DialogOperations.UPDATE;
    this._openSeasonDialog(season, this._lastOperation);
  }

  public deleteSeason(season: Season): void {
    let id = season.seasonId;
    this._seasonService.deleteSeason(id.toLocaleString()).then(() => {
      window.location.reload();
    });
  }

  public openCreateSeasonDialog(): void {
    this._lastOperation = DialogOperations.CREATE;
    this._openSeasonDialog(new Season(), this._lastOperation);
  }

  private _openSeasonDialog(season: Season, lastOperation: string): void {
    const dialogRef = this.dialog.open(SeasonDialogComponent, {
      width: '300px',
      data: {season: season, operation: lastOperation}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      if (this._lastOperation === DialogOperations.CREATE) {
        this._newSeason = result.season;
        this._newSeason.league = this.selectedLeague;
        this._newSeason.leagueId = this.selectedLeague.leagueId;

        this._seasonService.createSeason(this._newSeason).then(() => {
          window.location.reload();
        })
      } else if (this._lastOperation === DialogOperations.UPDATE) {
        this._seasonService.updateSeason(result.season);
      }
    });
  }

  public getLeagues(): League[] {
    return this._leagues;
  }

  public onLeaguesSelectionChange(league: League): void {
    this.selectedLeague = league;
    this._updateSeasonDataSource(league.leagueId);
  }

}
