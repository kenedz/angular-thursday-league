import {Component, OnInit, ViewChild} from '@angular/core';
import {User, Role, UsersService, RolesService} from "../../../core/services";
import {UserDialogComponent} from "../user-dialog/user-dialog.component";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {DialogOperations} from "../../models/DialogOperations";

@Component({
    selector: 'app-user-console',
    templateUrl: './user-console.component.html',
    styleUrls: ['./user-console.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ])
    ]
})
export class UserConsoleComponent implements OnInit {

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    private _lastOperation: string;
    private _emailMap: Map<string, string>;

    private _roles: Role[];

    private _newUser: User;
    private _users: User[];
    public usersDataSource: MatTableDataSource<User>;
    public usersTableColumns: string[] = ['userId', 'email', 'firstName', 'lastName', 'role'];
    public expandedUserElement: User | null;

    constructor(private _userService: UsersService, private _rolesService: RolesService, public dialog: MatDialog) {
    }

    ngOnInit() {
        this._initUsers();
        this._initRoles();
    }

    private _initUsers(): void {
        this._userService.getUsers().then(users => {
            this._users = users;

            this.usersDataSource = new MatTableDataSource<User>(this._users);
            this.usersDataSource.paginator = this.paginator;

            this._initEmailMap(this._users);
        });
    }

    private _initRoles(): void {
        this._rolesService.getRoles().then(roles => {
            this._roles = roles;
        })
    }

    private _initEmailMap(users: User[]): void {
        this._emailMap = new Map<string, string>();
        users.forEach(user => {
            this._emailMap.set(user.email, user.email);
        })
    }

    public updateUser(user: User): void {
        this._lastOperation = DialogOperations.UPDATE;
        this._openUserDialog(user, this._lastOperation);
    }

    public deleteUser(user: User): void {
        let id = user.userId;
        this._userService.deleteUser(id.toLocaleString()).then(data => {
            window.location.reload();
        });
    }

    public displayDeleteBtn(user: User): boolean {
        return user.role.role != Role.SUPERADMIN;
    }

    public openCreateUserDialog(): void {
        this._lastOperation = DialogOperations.CREATE;
        this._openUserDialog(new User(), this._lastOperation);
    }

    private _openUserDialog(user: User, operation: string) {

        const dialogRef = this.dialog.open(UserDialogComponent, {
            width: '300px',
            data: {user: user, roles: this._roles, operation: operation, emailMap: this._emailMap}
        });

        dialogRef.afterClosed().subscribe(result => {
            if (!result || this._isNotValid(result.user)) {
                return;
            }

            if (this._lastOperation === DialogOperations.CREATE) {
                this._newUser = result.user;

                this._userService.createUser(this._newUser).then(data => {
                    window.location.reload();
                })
            } else if (this._lastOperation === DialogOperations.UPDATE) {
                this._userService.updateUser(result.user);
            }
        });
    }

    private _isNotValid(user: User): boolean {
        return !user.email || !user.firstName || !user.lastName || !user.role;
    }

}
