import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Role} from '../../../core/services';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {TranslateStore} from '../../../core/local-store';
import {UserDialogInput} from "./models/UserDialogInput";
import {DialogOperations} from "../../models/DialogOperations";

@Component({
    selector: 'app-create-user-dialog',
    templateUrl: './user-dialog.component.html',
    styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {

    public operation: string;

    roleSelections: RoleSelection[];
    required: string;
    emailInvalid: string;
    maxLength: string;
    emailExists: string;
    titleCreate: string;
    titleUpdate: string;
    roleSelectionId: number;

    userFormGroup: FormGroup;

    constructor(public dialogRef: MatDialogRef<UserDialogComponent>, @Inject(MAT_DIALOG_DATA) public dialogInput: UserDialogInput,
                private _translateService: TranslateStore) {
        this.roleSelections = this.getRoleSelectionsFromRole(dialogInput.roles);
        this.operation = dialogInput.operation;
        if (!!dialogInput.user.role) {
            this.roleSelectionId = this.getSelectionId(dialogInput.user.role);
        }

        this.userFormGroup = new FormGroup({
            emailControl: new FormControl({value: '', disabled: this.isUpdate()}, [Validators.required, Validators.email,
                Validators.maxLength(200),
                this.existingEmailValidator()]),
            firstNameControl: new FormControl('', [Validators.required, Validators.maxLength(100)]),
            lastNameControl: new FormControl('', [Validators.required, Validators.maxLength(100)]),
            roleControl: new FormControl('', [Validators.required])
        });
    }

    ngOnInit(): void {
        this._initTranslations();
    }

    private _initTranslations(): void {
        this._translateService.getTranslation("page.adminconsole.userdialog.error.required").subscribe(data => {
            this.required = data;
        });
        this._translateService.getTranslation("page.adminconsole.userdialog.error.emailinvalid").subscribe(data => {
            this.emailInvalid = data;
        });
        this._translateService.getTranslation("page.adminconsole.userdialog.error.maxlength").subscribe(data => {
            this.maxLength = data;
        });
        this._translateService.getTranslation("page.adminconsole.userdialog.error.emailexists").subscribe(data => {
            this.emailExists = data;
        });
        this._translateService.getTranslation("page.adminconsole.userdialog.titlecreate").subscribe(data => {
            this.titleCreate = data;
        });
        this._translateService.getTranslation("page.adminconsole.userdialog.titleupdate").subscribe(data => {
            this.titleUpdate = data;
        });
    }

    public onCancelClick(): void {
        this.dialogRef.close();
    }

    getEmailErrorMessage() {
        let errorMsg = '';
        if (this.emailControl.hasError('required')) {
            errorMsg = this.required;
        } else if (this.emailControl.hasError('email')) {
            errorMsg = this.emailInvalid;
        } else if (this.emailControl.hasError('maxlength')) {
            errorMsg = this.maxLength;
        } else if (this.emailControl.hasError('existingEmail')) {
            errorMsg = this.emailExists;
        }
        return errorMsg;
    }

    getFirstNameErrorMessage() {
        let errorMsg = '';
        if (this.firstNameControl.hasError('required')) {
            errorMsg = this.required;
        } else if (this.firstNameControl.hasError('maxlength')) {
            errorMsg = this.maxLength;
        }

        return errorMsg;
    }

    getLastNameErrorMessage() {
        let errorMsg = '';
        if (this.lastNameControl.hasError('required')) {
            errorMsg = this.required;
        } else if (this.lastNameControl.hasError('maxlength')) {
            errorMsg = this.maxLength;
        }


        return errorMsg;
    }

    getRoleErrorMessage() {
        let errorMsg = '';
        if (this.roleControl.hasError('required')) {
            errorMsg = this.required;
        }

        return errorMsg;
    }

    get emailControl() {
        return this.userFormGroup.get('emailControl');
    }

    get firstNameControl() {
        return this.userFormGroup.get('firstNameControl');
    }

    get lastNameControl() {
        return this.userFormGroup.get('lastNameControl');
    }

    get roleControl() {
        return this.userFormGroup.get('roleControl');
    }

    public getTitle(): string {
        let title = '';
        if (this.operation === DialogOperations.CREATE) {
            title = this.titleCreate;
        } else if (this.operation === DialogOperations.UPDATE) {
            title = this.titleUpdate;
        }
        return title;
    }

    private getSelectionId(role: Role): number {
        var roleId1 = role.roleId;
        var selectionId: number;
        this.roleSelections.forEach(selection => {
            var roleId2 = selection.role.roleId;
            if (roleId1 === roleId2) {
                selectionId = selection.id;
            }
        });
        return selectionId;
    }

    private getRoleSelectionsFromRole(roles: Role[]): RoleSelection[] {
        let id = 1;
        var roleSelections = new Array<RoleSelection>();
        roles.forEach((element) => {
            let selection = new RoleSelection();
            selection.role = element;
            selection.id = id++;

            roleSelections.push(selection);
        });

        return roleSelections;
    }


    private getRoleFromRoleSelection(selectionId: number): Role {
        var role: Role;
        for (let selection of this.roleSelections) {
            var selectionId2 = selection.id;
            if (selectionId === selectionId2) {
                role = selection.role;
                break;
            }
        }

        return role;
    }

    onSubmit(): void {
        this.dialogInput.user.role = this.getRoleFromRoleSelection(this.roleSelectionId);
    }

    existingEmailValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            var existingEmail = this.dialogInput.emailMap.get(control.value);
            return !!existingEmail && this.operation === DialogOperations.CREATE ? {'existingEmail': {value: control.value}} :
                null;
        };
    }

    isUpdate(): boolean {
        return this.operation === DialogOperations.UPDATE;
    }

}

export class RoleSelection {
    id: number;
    role: Role;
}
