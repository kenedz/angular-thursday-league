import {User} from "../../../../core/services/users/models/User";
import {Role} from "../../../../core/services/roles/models/Role";

export interface UserDialogInput {
    user: User;
    roles: Role[];
    operation: string;
    emailMap: Map<string, string>;
}
