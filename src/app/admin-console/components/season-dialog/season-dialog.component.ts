import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TranslateStore} from "../../../core/local-store";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SeasonDialogInput} from "./models/SeasonDialogInput";
import {DialogOperations} from "../../models/DialogOperations";

@Component({
    selector: 'app-season-dialog',
    templateUrl: './season-dialog.component.html',
    styleUrls: ['./season-dialog.component.scss']
})
export class SeasonDialogComponent implements OnInit {

    public operation: string;
    public seasonFormGroup: FormGroup;

    private _titleCreate: string;
    private _titleUpdate: string;

    constructor(private _translateService: TranslateStore, @Inject(MAT_DIALOG_DATA) public dialogInput: SeasonDialogInput, public dialogRef: MatDialogRef<SeasonDialogComponent>) {
        this.operation = dialogInput.operation;

        this.seasonFormGroup = new FormGroup({
            seasonYearControl: new FormControl({value: ''}, [Validators.required, Validators.maxLength(20)]),
            seasonStartControl: new FormControl({value: ''}, [Validators.required, Validators.maxLength(20)]),
            seasonEndControl: new FormControl({value: ''}, [Validators.required, Validators.maxLength(20)])
        });
    }

    ngOnInit() {
        this._initTranslations();
    }

    private _initTranslations(): void {
        this._translateService.getTranslation("page.adminconsole.seasondialog.titlecreate").subscribe(data => {
            this._titleCreate = data;
        });
        this._translateService.getTranslation("page.adminconsole.seasondialog.titleupdate").subscribe(data => {
            this._titleUpdate = data;
        });
    }

    public getTitle(): string {
        let title = '';
        if (this.operation === DialogOperations.CREATE) {
            title = this._titleCreate;
        } else if (this.operation === DialogOperations.UPDATE) {
            title = this._titleUpdate;
        }
        return title;
    }

    public get seasonYearControl() {
        return this.seasonFormGroup.get('seasonYearControl');
    }

    public get seasonStartControl() {
        return this.seasonFormGroup.get('seasonStartControl');
    }

    public get seasonEndControl() {
        return this.seasonFormGroup.get('seasonEndControl');
    }

    public onSubmit(): void {
    }

    public onCancelClick(): void {
        this.dialogRef.close();
    }


}


