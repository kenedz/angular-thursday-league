import {Season} from "../../../../core/services/seasons/models/Season";

export interface SeasonDialogInput {
    season: Season;
    operation: string;
}
