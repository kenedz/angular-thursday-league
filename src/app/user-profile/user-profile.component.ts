import {Component, OnInit} from '@angular/core';
import {TranslateStore} from '../core/local-store';
import {AuthService, SocialUser} from 'angularx-social-login';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

    public user: SocialUser;
    private _loggedIn: boolean;

    constructor(private translateService: TranslateStore, private authService: AuthService) {
    }

    public ngOnInit() {
        this.authService.authState.subscribe((user) => {
            this.user = user;
            this._loggedIn = (user != null);
        });
    }
}
